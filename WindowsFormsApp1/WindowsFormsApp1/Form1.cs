﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Vasyl Husarovskiy");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Мій досвід програмування:" +Environment.NewLine +"Досвіду як такого немає");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show ("Understanding Git" + Environment.NewLine + "https://www.w3schools.com/js/default.asp");
        }
    }
}
